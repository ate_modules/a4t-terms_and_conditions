Terms & Conditions
##################

With this module you can define some article. After that you can build your
Terms & Conditions for Sale, Purchase or Use by choosing from the list of
previously defined articles.

Using
*****

  * To define articles got to : Parties -> Configuration -> Company -> Terms & Conditions -> Article (T&C)
  * To build your Term & Conditions go to : Parties -> Configuration -> Company -> Terms & Conditions
