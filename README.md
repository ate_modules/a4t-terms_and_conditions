# terms_and_conditions

With this module you can define some article. After that you can build your
Terms & Conditions for Sale, Purchase or Use by choosing from the list of
previously defined articles.

## Installing

See INSTALL

## Support

If you encounter any problems with this module, please don't hesitate to ask
questions on the Adiczion support mail address :

  support@adiczion.net

If you encounter any problems with Tryton, please don't hesitate to ask
questions on the Tryton bug tracker, mailing list, wiki or IRC channel:

  [http://bugs.tryton.org/](http://bugs.tryton.org/)
  [http://groups.tryton.org/](http://groups.tryton.org/)
  [http://wiki.tryton.org/](http://wiki.tryton.org/)
  [irc://irc.freenode.net/tryton](irc://irc.freenode.net/tryton)

## License

See LICENSE

## Copyright

See COPYRIGHT

For more information about us please visit the Adiczion web site:

  [http://adiczion.com](http://adiczion.com)

For more information about Tryton please visit the Tryton web site:

  [http://www.tryton.org/](http://www.tryton.org/)
